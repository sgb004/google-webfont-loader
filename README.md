# Google Webfont Loader

A simple script to load Google webfonts.

This package use [webfont.js](https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js).

## Installation

```sh
npm install google-webfont-loader
```

## Usage

```javascript
// @ts-ignore
import { webFontLoader } from 'google-webfont-loader';

webFontLoader({
	google: {
		families: ['Raleway:400,700&display=swap'],
	},
});
```
