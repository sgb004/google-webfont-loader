declare module 'google-webfont-loader' {
	export function webFontLoader(webFontConfig: Object): Promise<void>;
}
